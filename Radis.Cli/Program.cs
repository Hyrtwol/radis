﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;

namespace Radis.Cli
{
    class Program
    {
        private static readonly string Title = Assembly.GetEntryAssembly().GetName().Name;

        public const int DefaultConsoleWidth = 120;
        public const int DefaultConsoleHeight = 25;

        static void Main(string[] args)
        {
            ConfigureConsole();

            WriteLine(Colors.Info, "Redis.Cli");
            Write(Colors.Prompt, "<press return to connect>");
            var hostname = Console.ReadLine();
            if (string.IsNullOrEmpty(hostname) && args.Length > 0)
            {
                hostname = args[0];
            }
            if (string.IsNullOrEmpty(hostname))
            {
                hostname = "127.0.0.1";
            }

            try
            {
                WriteLine(Colors.Info, "Connecting to {0}", hostname);
                using (var redis = new RedisClient(hostname, RedisClient.DefaultPort))
                {
                    WriteHelp(redis);
                    for (string cmd = GetCmd(); cmd != "!"; cmd = GetCmd())
                    {
                        var redisCommand = cmd.ToLower();
                        if (redisCommand == "exit") break;
                        if (redisCommand == "?" || redisCommand == "help")
                        {
                            WriteHelp(redis);
                            continue;
                        }
                        try
                        {
                            ExeCmd(redis, SplitCommand(cmd).ToArray());
                        }
                        catch (RedisReplyException ex)
                        {
                            WriteLine(Colors.Error, "Reply error: {0}",  ex.Message);
                        }
                        catch (RedisException ex)
                        {
                            WriteLine(Colors.Error, "Error: {0}", ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteLine(Colors.Error, ex.Message);
            }

            WriteLine(Colors.Info, "Done.");
            Write(Colors.Prompt, "<press return to exit>");
            Console.ReadLine();
        }

        private static void ConfigureConsole()
        {
            Console.Title = Title;
            Console.SetWindowSize(DefaultConsoleWidth, DefaultConsoleHeight);
            Console.SetBufferSize(DefaultConsoleWidth, 100);

            var windowPosition = ConfigurationManager.AppSettings["WindowPosition"];
            if (!string.IsNullOrEmpty(windowPosition))
            {
                var vals = windowPosition.Split(' ').Select(int.Parse).ToArray();
                if (vals.Length == 2) ConsoleUtils.SetWindowPos(vals[0], vals[1]);
            }
        }

        private static void WriteHelp(RedisClient redis)
        {
            WriteLine(Colors.Info, "Connected to {0}", redis.RemoteEndPoint);
            WriteLine(Colors.Info, "Type ! or exit to quit");
            WriteLine(Colors.Info, "See http://redis.io/commands for full command list");
        }

        private static IEnumerable<object> SplitCommand(string cmd)
        {
            return cmd.Split(' ').Where(value => !string.IsNullOrEmpty(value));
        }

        private static void ExeCmd(RedisClient redis, object[] data)
        {
            if (data.Length <= 0) return;
            var reply = redis.Request(data);
            if (reply != null)
                foreach (var line in reply)
                {
                    WriteLine(Colors.Result, line);
                }
        }

        private static string GetCmd()
        {
            string cmd;
            do
            {
                Write(Colors.Prompt, ">");
                cmd = Console.ReadLine();
            } while (string.IsNullOrEmpty(cmd));
            return cmd;
        }

        protected static void Write(ConsoleColor color, string format, params Object[] arg)
        {
            Console.ForegroundColor = color;
            Console.Write(format, arg);
            Console.ForegroundColor = Colors.Default;
        }

        protected static void WriteLine(ConsoleColor color, string format, params Object[] arg)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(format, arg);
            Console.ForegroundColor = Colors.Default;
        }

        protected static void WriteLine(ConsoleColor color, object value)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(value);
            Console.ForegroundColor = Colors.Default;
        }

        private static class Colors
        {
            internal static readonly ConsoleColor Default = Console.ForegroundColor;
            internal const ConsoleColor Error = ConsoleColor.Red;
            internal const ConsoleColor Prompt = ConsoleColor.Gray;
            internal const ConsoleColor Info = ConsoleColor.Green;
            internal const ConsoleColor Result = ConsoleColor.Yellow;
        }

        private static void TestRedis(RedisClient redis)
        {
            const string lastModifiedKey = "last-modified";
            redis.SET(lastModifiedKey, "Time is " + DateTime.Now);
            Console.WriteLine("GET '{0}' = '{1}'", lastModifiedKey, redis.GET(lastModifiedKey));

            redis.MSET("rip", 1, "rap", 2, "rup", 3);

            Console.WriteLine("MGET:");
            var mget = redis.MGET("rip", "rap", "rup", "rop");
            for (int i = 0; i < mget.Length; i++)
            {
                Console.WriteLine("  [{0}]='{1}'", i, mget[i]);
            }

            Console.WriteLine("LRANGE:");
            var range = redis.LRANGE("litq", 0, 10);
            for (int i = 0; i < range.Length; i++)
            {
                Console.WriteLine("  [{0}]='{1}'", i, range[i]);
            }
            Console.WriteLine("RPUSH:  {0}", redis.RPUSH("litq", "tick" + Environment.TickCount));
            Console.WriteLine("LLEN:   {0}", redis.LLEN("litq"));
            Console.WriteLine("LINDEX: {0}", redis.LINDEX("litq", 1));

            Console.WriteLine("PING:   {0}", redis.PING());

            //redis.SHUTDOWN();
        }

    }
}
