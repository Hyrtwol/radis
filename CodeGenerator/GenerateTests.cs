﻿using System;
using System.IO;
using System.Text;
using NUnit.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CodeGenerator
{
    [TestFixture]
    public class GenerateTests
    {
        private const string CommandsJsonFile = @"..\..\commands.json";

        [Test]
        public void ConvertJson()
        {
            var code = ConvertJsonToCode(CommandsJsonFile);
            Console.Write(code);
        }

        [Ignore]
        [Test]
        public void ConvertJsonToFile()
        {
            const string csFile = @"..\..\..\Radis\IRedis.Generated.cs";

            var code = ConvertJsonToCode(CommandsJsonFile);
            Console.Write(code);
            File.WriteAllText(csFile, code);
        }

        private static string ConvertJsonToCode(string jsonFile)
        {
            var json = File.ReadAllText(jsonFile);
            var root = (JObject) JsonConvert.DeserializeObject(json);
            var sb = new StringBuilder();

            sb.AppendLine("using key = System.String;");
            sb.AppendLine("using pattern = System.String;");
            sb.AppendLine("using integer = System.Int32;");
            sb.AppendLine("using posix_time = System.DateTime;");
            sb.AppendLine();

            sb.AppendLine("namespace Radis");
            sb.AppendLine("{");
            
            sb.AppendLine("    public interface IRedisAlt");
            sb.AppendLine("    {");

            var ind = "        ";

            foreach (var value in root.Properties())
            {
                //Console.WriteLine(value.Name);
                var val = value.Value;
                var summary = val.Value<string>("summary");
                var complexity = val.Value<string>("complexity");

                sb.AppendLine(ind+"/// <summary>");
                sb.AppendLine(ind + "/// " + summary);
                if (!string.IsNullOrEmpty(complexity)) sb.AppendLine(ind + "/// <remarks>Complexity :" + complexity + "</remarks>");
                sb.AppendLine(ind + "/// </summary>");

                sb.Append(ind);
                //sb.Append("public ");
                sb.Append("void ");
                sb.Append(value.Name.Replace(" ", "_"));
                sb.Append("(");
                var arguments = val["arguments"];
                //Console.WriteLine("arguments: " + arguments);
                if (arguments != null)
                {
                    var notFirst = false;
                    foreach (var argument in arguments)
                    {
                        //Console.WriteLine("argument: {0} ({1})", argument["name"], argument["type"]);
                        if (notFirst) sb.AppendFormat(", ");
                        else notFirst = true;
                        var name = argument["name"].ToString();
                        var type = argument["type"].ToString();
                        sb.AppendFormat("{0} {1}", type.Replace(" ", "_"), name.Replace(":", "_").Replace("-", "_"));
                    }
                }
                sb.AppendLine(");");
                sb.AppendLine();
                //sb.AppendLine(ind+"{");
                //sb.AppendLine(ind + "}");
            }

            sb.AppendLine("    }");
            sb.AppendLine("}");

            var code = sb.ToString();
            return code;
        }
    }
}