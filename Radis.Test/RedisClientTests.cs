﻿using System;
using System.IO;
using NUnit.Framework;

namespace Radis.Test
{
    [TestFixture]
    public class RedisClientTests
    {
        [Test]
        public void DefaultPort()
        {
            Assert.AreEqual(6379, RedisClient.DefaultPort);

            Console.WriteLine("Personal:             {0}", Environment.GetFolderPath(Environment.SpecialFolder.Personal));
            Console.WriteLine("LocalApplicationData: {0}", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));

            var localRedisPath = Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.LocalApplicationData), "Redis");

            Console.WriteLine("GetFolderPath:        {0}", localRedisPath);

        }
    }
}