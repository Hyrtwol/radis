﻿using System.Diagnostics;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace Radis.Test
{
    [TestFixture]
    public class ProtocolRequestTests
    {
        //[Test]
        //public void CreateRequest()
        //{
        //    var requestStr = RedisClient.CreateRequest("SET", "mykey", "myvalue");
        //    Debug.WriteLine(requestStr);
        //    Assert.AreEqual("*3\r\n$3\r\nSET\r\n$5\r\nmykey\r\n$7\r\nmyvalue\r\n", requestStr);
        //}


        [Test]
        public void WriteRequest_SET()
        {
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                RedisClient.WriteRequest(ms, "SET", "mykey", "myvalue");
                int numBytes = (int) ms.Length;
                ms.Position = 0;
                buffer = new byte[numBytes];
                ms.Read(buffer, 0, numBytes);
            }

            Assert.IsNotNull(buffer);
            Assert.Greater(buffer.Length, 0);

            var requestStr = Encoding.ASCII.GetString(buffer);
            Debug.WriteLine(requestStr);
            Assert.AreEqual("*3\r\n$3\r\nSET\r\n$5\r\nmykey\r\n$7\r\nmyvalue\r\n",
                            requestStr);
        }

        [Test]
        public void WriteRequest_MSET()
        {
            byte[] buffer;
            using (var ms = new MemoryStream())
            {
                RedisClient.WriteRequest(ms, "MSET", "RIP", 1, "RAP", 2, "RUP", 3);
                int numBytes = (int) ms.Length;
                ms.Position = 0;
                buffer = new byte[numBytes];
                ms.Read(buffer, 0, numBytes);
            }

            Assert.IsNotNull(buffer);
            Assert.Greater(buffer.Length, 0);

            var requestStr = Encoding.ASCII.GetString(buffer);
            Debug.WriteLine(requestStr);
            Assert.AreEqual("*7\r\n$4\r\nMSET\r\n$3\r\nRIP\r\n$1\r\n1\r\n$3\r\nRAP\r\n$1\r\n2\r\n$3\r\nRUP\r\n$1\r\n3\r\n",
                            requestStr);
        }
    }
}