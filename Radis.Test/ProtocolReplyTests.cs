﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;

namespace Radis.Test
{
    [TestFixture]
    public class ProtocolReplyTests
    {
        [Test]
        public void ParseReplyMultiBulk()
        {
            const string reply =
                "*3\r\n$3\r\n" +
                "ABC" +
                "\r\n$3\r\n" +
                "DEF" +
                "\r\n$3\r\n" +
                "GHI" +
                "\r\n";

            var ary = RedisClient.ParseReply(reply);
            var aryStr = string.Join(";", ary);
            Debug.WriteLine(aryStr);
            Assert.AreEqual("ABC;DEF;GHI", aryStr);

            //reply = "$14\r\nThomas la Cour\r\n";
        }

        [Test]
        public void ParseReplyBulk()
        {
            const string reply = "$14\r\nThomas la Cour\r\n";
            var ary = RedisClient.ParseReply(reply);
            Assert.IsNotNull(ary);
            var aryStr = string.Join(";", ary);
            Debug.WriteLine(aryStr);
            Assert.AreEqual("Thomas la Cour", aryStr);
        }

        [Test]
        public void ParseReplyInteger()
        {
            const string reply = ":14\r\n";
            var ary = RedisClient.ParseReply(reply);
            Assert.IsNotNull(ary);
            Debug.WriteLine(string.Join(";", ary));
            Assert.AreEqual(1, ary.Length);
            Assert.AreEqual(14, ary[0]);
        }
    }
}
