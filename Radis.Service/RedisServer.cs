﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Radis.Service
{
    public class RedisServer
    {
        public const string RedisServerExe = "redis-server.exe";
        public const string RedisCliExe = "redis-cli.exe";
        
        private readonly CancellationTokenSource _cts;
        private Process _process;

        private int _port;
        private readonly string _path;
        private readonly string _redisPath;
        private readonly string _redisServerFullPath;
        private readonly string _redisCliFullPath;

        //private RedisClient redis;

        public RedisServer()
        {
            var codeBase = typeof(RedisServer).Assembly.CodeBase;
            var uri = new Uri(codeBase);
            _path = Path.GetDirectoryName(uri.LocalPath);
            if (string.IsNullOrEmpty(_path)) throw new NullReferenceException("Unable to get path for " + uri.LocalPath);
            _redisPath = Path.Combine(_path, Environment.Is64BitProcess ? "redisbin64" : "redisbin");
            _redisServerFullPath = Path.Combine(_redisPath, RedisServerExe);
            _redisCliFullPath = Path.Combine(_redisPath, RedisCliExe);
            _cts = new CancellationTokenSource();
            _process = null;
        }

        public void Start()
        {
            var arguments = Environment.GetCommandLineArgs();
            if (arguments.Length > 2) Exit("Too many arguments");
            
            Console.WriteLine("Starting...");

            if (!File.Exists(_redisServerFullPath)) Exit("Couldn't find " + _redisServerFullPath);
            if (!File.Exists(_redisCliFullPath)) Exit("Couldn't find " + _redisCliFullPath);


            string configPath;

            var redisConfPath = ConfigurationManager.AppSettings["RedisConf"];
            if (!string.IsNullOrEmpty(redisConfPath))
            {
                configPath = Environment.ExpandEnvironmentVariables(redisConfPath); // e.g. "%E247HOME%\redis.conf"
            }
            else
            {
                configPath = Path.Combine(_path, "redis.conf");
            }
            
            _process = StartRedis(configPath);
            if (_process.Start())
            {
                Console.WriteLine("Redis process started...");
                
                var token = _cts.Token;
                Task.Factory.StartNew(() =>
                {
                    string line;
                    while (!token.IsCancellationRequested && (line = _process.StandardOutput.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                    }

                }, token);

                Task.Factory.StartNew(() =>
                {
                    while (!token.IsCancellationRequested)
                    {
                        Thread.Sleep(5000);
                        if (token.IsCancellationRequested) break;
                        token.ThrowIfCancellationRequested();
                        WithRedisDo(redis => Console.WriteLine("PING:   {0}", redis.PING()));
                    }
                }, token);
            }
            else Exit("Failed to start Redis process");
        }

        public void Stop()
        {
            Console.WriteLine("Sending Shutdown...");
            WithRedisDo(redis => redis.SHUTDOWN());
            
            Console.WriteLine("Waiting for exit...");
            if (!_process.WaitForExit(5000))
            {
                Console.WriteLine("Killing redis process");
                _process.Kill();
            }
            Console.WriteLine("Redis process exited. ExitCode=" + _process.ExitCode);
            _process.Dispose();
            _process = null;
            
            _cts.Cancel();
        }

        private Process StartRedis(string configPath)
        {
            if (string.IsNullOrEmpty(configPath)) throw new ArgumentNullException("configPath");
            FindPort(configPath);
            var startInfo = new ProcessStartInfo(_redisServerFullPath)
                {
                    // Workaround for spaces in configuration filename.
                    Arguments = Path.GetFileName(configPath),
                    WorkingDirectory = Path.GetDirectoryName(configPath),
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true
                };
            return new Process {StartInfo = startInfo};
        }

        private void FindPort(string path)
        {
            using (var reader = new StreamReader(path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.IndexOf("port") == 0)
                    {
                        _port = int.Parse(line.Substring(5, line.Length - 5));
                        break;
                    }
                }
                if (_port == 0)
                    Exit("Couldn`t find Redis port in config file");
            }
        }
        
        void WithRedisDo(Action<RedisClient> action)
        {
            try
            {
                using (var redis = new RedisClient("127.0.0.1"))
                {
                    action(redis);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void Exit(string message)
        {
            if (Environment.UserInteractive)
            {
                Console.WriteLine(message);
                //Environment.Exit(-1);
            }
            //else
            {
                //File.WriteAllText(Path.Combine(_path, "error.txt"), message);
                throw new ApplicationException(message);
            }
        }
    }
}