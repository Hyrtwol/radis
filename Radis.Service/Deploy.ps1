﻿# These variables should be set via the Octopus web portal:
#
#   $TexlUser
#   $ServiceStartup      - Automatic or Manual
#
try 
{
    $ServiceName = "$OctopusPackageName"
    $ServiceExecutable = "$OctopusPackageName.exe"
    $ServiceConfiguration = "$OctopusPackageName.exe.config"

    #
    # Create E247HOME path
    #
    Write-Host "DefaultHomePath: '$DefaultHomePath'"
    #if(!$DefaultHomePath) { $DefaultHomePath = "C:\E247HOME" }
    $homePath = [Environment]::GetEnvironmentVariable("E247HOME", "Machine")
    #if( !$homePath)
    #{
    #	Write-Host "Creating system envrionment variable E247HOME with default value '$DefaultHomePath'"
    #	[Environment]::SetEnvironmentVariable("E247HOME", $DefaultHomePath, "Machine")
    #	$homePath = [Environment]::GetEnvironmentVariable("E247HOME", "Machine")
    #}
    if( !$homePath)
    {
	    throw "Missing system envrionment variable E247HOME"
    }
    Write-Host "E247HOME = '$homePath'"

    # Create directory if it does not exists
    if (!(Test-Path $homePath)) {
	    #[void](new-item $homePath -itemType directory)
	    $newDir = [System.IO.Directory]::CreateDirectory($homePath)
	    Write-Host "Created directory '$newDir'"
    }
    #new-item -path $homePath -name notexist -type directory -force

    #
    # Setup the service
    #
    if(!$ServiceStartup) { $ServiceStartup = "Automatic" }
    Write-Host "ServiceStartup: '$ServiceStartup'"

    $service = Get-Service $ServiceName -ErrorAction SilentlyContinue
    $fullPath = Resolve-Path $ServiceExecutable

    if (! $service)
    {
        Write-Host "The service will be installed"
        Write-Host "Service name:" $ServiceName "Startup type:" $ServiceStartup
        New-Service -Name $ServiceName -BinaryPathName $fullPath -StartupType $ServiceStartup 
    }
    else
    {
        $st = "auto"
        if ($ServiceStartup -eq "Manual")
	    {
	        $st = "demand"
	    }

        Write-Host "The service will be stopped and reconfigured"
        Stop-Service $ServiceName -Force
	    Start-Sleep -s 60    
        Write-Host "Service name:" $ServiceName "Startup type:" $st
	    & "sc.exe" config $ServiceName binPath= $fullPath start= $st | Write-Host
    }

    # set user and password - note the space after obj= is intentional!!!!
    # Write-Host "Setting config:" $ServiceName "User:" $serviceUser
    # & "sc.exe" config $ServiceName obj= $serviceUser password= "$servicePass" | Write-Host

    if ($ServiceStartup -eq "Automatic")
    {
        Write-Host "Starting service '$ServiceName'"
	    Start-Service $ServiceName
    }
}
catch
{
    Write-Host $error[0]
    Exit 247
}