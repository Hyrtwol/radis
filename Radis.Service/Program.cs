﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;

namespace Radis.Service
{
    static class Program
    {
        private static readonly string Title = Assembly.GetEntryAssembly().GetName().Name;

        public const int DefaultConsoleWidth = 120;
        public const int DefaultConsoleHeight = 25;

        static void Main()
        {
            Console.CancelKeyPress += (sender, args) => { args.Cancel = true; };
            Run(new RedisService());
        }

        private static void Run(RedisService texlService)
        {
            if (Environment.UserInteractive)
            {
                ConfigureConsole();
                texlService.Start();
                Console.WriteLine("(Press ESC to stop)");
                WairForEscape();
                texlService.Stop();
                Console.WriteLine("(Press ESC to exit)");
                WairForEscape();
            }
            else
            {
                ServiceBase.Run(texlService);
            }
        }

        private static void WairForEscape()
        {
            ConsoleKey key;
            do
            {
                if (Console.KeyAvailable) key = Console.ReadKey(true).Key;
                else
                {
                    key = 0;
                    System.Threading.Thread.Sleep(200);
                }
            } while (key != ConsoleKey.Escape);
        }

        private static void ConfigureConsole()
        {
            Console.Title = Title;
            Console.SetWindowSize(DefaultConsoleWidth, DefaultConsoleHeight);
            Console.SetBufferSize(DefaultConsoleWidth, 100);

            var windowPosition = ConfigurationManager.AppSettings["WindowPosition"];
            if (!string.IsNullOrEmpty(windowPosition))
            {
                var vals = windowPosition.Split(' ').Select(int.Parse).ToArray();
                if (vals.Length == 2) ConsoleUtils.SetWindowPos(vals[0], vals[1]);
            }
        }
    }
}
