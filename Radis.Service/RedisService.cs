﻿using System;
using System.ServiceProcess;

namespace Radis.Service
{
    partial class RedisService : ServiceBase
    {
        private readonly RedisServer _redisServer;

        public RedisService()
        {
            InitializeComponent();
            _redisServer = new RedisServer();
        }
        
        public void Start()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            _redisServer.Start();
        }

        protected override void OnStop()
        {
            _redisServer.Stop();
        }
    }
}
