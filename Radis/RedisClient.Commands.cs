﻿// ReSharper disable InconsistentNaming

using System;

namespace Radis
{
    // http://redis.io/commands

    public partial class RedisClient
    {
        public string PING()
        {
            return (string)Request("PING")[0];
        }

        public void SHUTDOWN()
        {
            var ns = _client.GetStream();
            WriteRequest(ns, new object[] { "SHUTDOWN" });
        }

        public void QUIT()
        {
            var reply = (string)Request("QUIT")[0];
            if (reply != "OK") throw new RedisException(reply);
        }

        public void SET(string key, string value)
        {
            var reply = (string)Request("SET", key, value)[0];
            if(reply != "OK") throw new RedisException(reply);
        }

        //public void MSET(string key, IEnumerable<KeyValuePair<string, string>> values)

        public void MSET(params object[] values)
        {
            var num = values.Length;
            var data = new object[num+1];
            data[0] = "MSET";
            Array.Copy(values, 0, data, 1, num);
            var reply = (string)Request(data)[0];
            if (reply != "OK") throw new RedisException(reply);
        }

        public string GET(string key)
        {
            return (string)Request("GET", key)[0];
        }

        public string[] MGET(params object[] values)
        {
            var num = values.Length;
            var data = new object[num + 1];
            data[0] = "MGET";
            Array.Copy(values, 0, data, 1, num);
            return (string[])Request(data);
        }

        public string[] LRANGE(string key, int start, int stop)
        {
            return (string[])Request("LRANGE", key, start, stop);
        }

        public int RPUSH(string key, string value)
        {
            return (int)Request("RPUSH", key, value)[0];
        }

        public int LLEN(string key)
        {
            return (int)Request("LLEN", key)[0];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public string LINDEX(string key, int index)
        {
            return (string)Request("LINDEX", key, index)[0];
        }

        public int LINDEX(string key, string index)
        {
            return (int)Request("LINDEX", key, index)[0];
        }
    }
}