﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Radis
{
    // http://redis.io/commands
    // http://redis.io/topics/protocol

    /* A client connects to a Redis server creating a TCP connection to the port 6379.
     * Every Redis command or data transmitted by the client and the server is
     * terminated by \r\n (CRLF). */

    public partial class RedisClient : IDisposable //, IRedis
    {
        public const int DefaultPort = 6379;
        private const int ReceiveBufferSize = 1024;
        private static readonly Encoding Encoding = Encoding.UTF8;

        private readonly string _hostname;
        private readonly int _port;
        private TcpClient _client;
        //private Encoding _encoding;

        public RedisClient(string hostname, int port = DefaultPort, bool autoConnect = true)
        {
            _hostname = hostname;
            _port = port;
            //_encoding = Encoding.UTF8;
            if(autoConnect) Connect();

            //_client.Client.LocalEndPoint
        }

        public EndPoint LocalEndPoint
        {
            get { return _client.Client.LocalEndPoint; }
        }

        public EndPoint RemoteEndPoint
        {
            get { return _client.Client.RemoteEndPoint; }
        }

        public void Dispose()
        {
            if (_client != null)
            {
                _client.Close();
            }
        }

        public void Connect()
        {
            if (_client != null) throw new RedisException("TcpClient already exists");
            try
            {
                _client = new TcpClient(_hostname, _port);
            }
            catch (SocketException)
            {
                _client = null;
                throw;
            }
        }

        public object[] Request(params object[] data)
        {
            var ns = _client.GetStream();
            WriteRequest(ns, data);
            var reply = ReadReply(ns);
            try
            {
                return ParseReply(reply);
            }
            catch (RedisException ex)
            {
                throw new RedisReplyException(reply, ex);
            }
        }

        //private static void Send(Stream ns, object[] data)
        //{
        //    WriteRequest(ns, data);
        //}

        private static char[] ReadReply(Stream ns)
        {
            var data = new byte[ReceiveBufferSize];
            var recv = ns.Read(data, 0, data.Length);
            return Encoding.GetChars(data, 0, recv);
        }
    }
}
