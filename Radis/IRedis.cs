﻿// ReSharper disable InconsistentNaming

namespace Radis
{
    public interface IRedis
    {
        void SET(string key, string value);
        string GET(string key);
        string[] LRANGE(string key, int start, int stop);
        int RPUSH(string key, string value);
        int LLEN(string key);
        string LINDEX(string key, int index);
        int LINDEX(string key, string index);
    }
}