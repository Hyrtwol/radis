﻿using System;
using System.IO;
using System.Text;

namespace Radis
{
    // http://redis.io/topics/protocol

    public partial class RedisClient
    {
        private enum ReplyKind
        {
            Status = '+',
            Error = '-',
            Integer = ':',
            Bulk = '$',
            MultiBulk = '*'
        }

        private const byte CR = (byte) '\r';
        private const byte LF = (byte) '\n';
        
        internal static void WriteRequest(Stream ns, params object[] data)
        {
            /*
        
            *<number of arguments> CR LF
            $<number of bytes of argument 1> CR LF
            <argument data> CR LF
            ...
            $<number of bytes of argument N> CR LF
            <argument data> CR LF
        
            */

            var numArgs = data.Length;
            if (numArgs < 1) throw new ArgumentOutOfRangeException("data", "Must have at least one value");

            WriteReplyKind(ns, ReplyKind.MultiBulk);
            WriteInteger(ns, numArgs);

            for (int i = 0; i < numArgs; i++)
            {
                object value = data[i];
                //if (value is int)
                //{
                //    int valueInt = (int)value;
                //    WriteReplyKind(ns, ReplyKind.Integer);
                //    WriteInteger(ns, valueInt);
                //}
                //else
                WriteString(ns, value.ToString());
            }

            ns.Flush();
        }

        private static void WriteReplyKind(Stream ns, ReplyKind replyKind)
        {
            ns.WriteByte((byte) replyKind);
        }

        private static void WriteCRLF(Stream ns)
        {
            ns.WriteByte(CR);
            ns.WriteByte(LF);
        }

        private static void WriteInteger(Stream ns, int value)
        {
            var buffer = Encoding.ASCII.GetBytes(value.ToString("D"));
            ns.Write(buffer, 0, buffer.Length);
            WriteCRLF(ns);
        }
        
        private static void WriteString(Stream ns, string value)
        {
            WriteBulk(ns, Encoding.GetBytes(value));
        }

        private static void WriteBulk(Stream ns, byte[] buffer)
        {
            var numBytes = buffer.Length;
            WriteReplyKind(ns, ReplyKind.Bulk);
            WriteInteger(ns, numBytes);
            ns.Write(buffer, 0, numBytes);
            WriteCRLF(ns);
        }

        internal static object[] ParseReply(string reply)
        {
            return ParseReply(reply.ToCharArray());
        }

        internal static object[] ParseReply(Stream reply)
        {
            object[] res = null;
            var replyKind = (ReplyKind) reply.ReadByte();
            switch (replyKind)
            {
                case ReplyKind.Error:
                    //var error = ParseError(reply);
                    //throw new RedisException(error);
                case ReplyKind.Status:
                    //res = ParseStatus(reply);
                    break;
                case ReplyKind.MultiBulk:
                    //int len = ParseInteger(reply);
                    //res = ParseBulk(reply);
                    break;
                case ReplyKind.Integer:
                    //var strVal = ParseInteger(reply);
                    //res = new object[] { strVal };
                    break;
                case ReplyKind.Bulk:
                    //res = ParseBulk(reply);
                    break;
                default:

                    throw new RedisException(string.Format("Unsupported reply kind '{0}'", replyKind));
            }
            return res;
        }

        private static object[] ParseReply(char[] reply)
        {
            /*
             
            Redis will reply to commands with different kinds of replies. It is always possible to detect the kind of reply from the first byte sent by the server:
            
            In a Status Reply the first byte of the reply is "+"
            In an Error Reply the first byte of the reply is "-"
            In an Integer Reply the first byte of the reply is ":"
            In a Bulk Reply the first byte of the reply is "$"
            In a Multi Bulk Reply the first byte of the reply s "*"

            */

            object[] res;
            unsafe
            {
                int numChars = reply.Length;
                fixed (char* replyChars = reply)
                {
                    res = ParseReply(replyChars, numChars);
                }
            }
            return res;
        }

        private static unsafe object[] ParseReply(char* p, int numChars)
        {
            object[] res;
            var pEnd = p + numChars;
            switch ((ReplyKind) (*p))
            {
                case ReplyKind.Error:
                    var error = ParseError(ref p, pEnd);
                    throw new RedisException(error);
                case ReplyKind.Status:
                    res = ParseStatus(ref p, pEnd);
                    break;
                case ReplyKind.MultiBulk:
                    int len = ParseInteger(ref p, pEnd);
                    res = ParseBulk(len, p, pEnd);
                    break;
                case ReplyKind.Integer:
                    var strVal = ParseInteger(ref p, pEnd);
                    res = new object[] {strVal};
                    break;
                case ReplyKind.Bulk:
                    res = ParseBulk(1, p, pEnd);
                    break;
                default:
                    throw new RedisException(string.Format("Unsupported reply kind '{0}'", *p));
            }
            return res;
        }

        private static unsafe string ParseError(ref char* p, char* pEnd)
        {
            p++;
            int numChars = 0;
            var ps = p;
            while ((p < pEnd) && (*p != '\r'))
            {
                numChars++;
                p++;
            }
            p++;
            AssertChar(*p, '\n');
            p++;
            return new string(ps, 0, numChars);
        }

        private static unsafe object[] ParseStatus(ref char* p, char* pEnd)
        {
            p++;
            int numChars = 0;
            var ps = p;
            while ((p < pEnd) && (*p != '\r'))
            {
                numChars++;
                p++;
            }
            p++;
            AssertChar(*p, '\n');
            p++;
            return new[] {new string(ps, 0, numChars)};
        }

        private static unsafe object[] ParseBulk(int len, char* p, char* pEnd)
        {
            var res = new string[len];
            int idx = 0;
            while ((p < pEnd) && (idx < len) && ((ReplyKind) (*p) == ReplyKind.Bulk))
            {
                res[idx++] = ParseString(ref p, pEnd);
            }
            return res;
        }

        private static unsafe string ParseString(ref char* p, char* pEnd)
        {
            var dataLen = ParseInteger(ref p, pEnd);
            if (dataLen < 0)
            {
                return null;
            }
            var strVal = new string(p, 0, dataLen);
            p += dataLen;
            AssertChar(*p, '\r');
            p++;
            AssertChar(*p, '\n');
            p++;
            return strVal;
        }

        private static unsafe int ParseInteger(ref char* p, char* pEnd)
        {
            p++;
            int value = 0, sign = 1;
            if (((p < pEnd) && (*p == '-')))
            {
                sign = -1;
                p++;
            }
            while ((p < pEnd) && (*p != '\r'))
            {
                value = (value*10) + (*p - '0');
                p++;
            }
            p++;
            AssertChar(*p, '\n');
            p++;
            return sign * value;
        }

        static unsafe void AssertChar(char actual, char expected)
        {
            if (expected != actual)
            {
                throw new RedisException(string.Format("Char expected '{0}' actual '{1}'", expected, ToHexIfNeeded(actual)));
            }
        }

        private static string ToHexIfNeeded(char actual)
        {
            if (actual < 32) return string.Format("#{0:X2}", (int)actual);
            return actual.ToString();
        }

        //private static string Receive(this Stream ns)
        //{
        //    var data = new byte[ReceiveBufferSize];
        //    var recv = ns.Read(data, 0, data.Length);
        //    return Encoding.GetString(data, 0, recv);
        //}

        //private static char[] Receive(this Stream ns)
        //{
        //    var data = new byte[ReceiveBufferSize];
        //    var recv = ns.Read(data, 0, data.Length);
        //    return Encoding.GetChars(data, 0, recv);
        //}

        

        //public static IEnumerable ParseReply(char[] reply)
        //{
        //    return new Reply(reply);
        //}
        //class ReplyEnumerator : IEnumerator
        //{
        //    private char[] _reply;
        //    public ReplyEnumerator(char[] reply)
        //    {
        //        _reply = reply;
        //    }
        //    public bool MoveNext()
        //    {
        //        throw new NotImplementedException();
        //    }
        //    public void Reset()
        //    {
        //        throw new NotImplementedException();
        //    }
        //    public object Current { get; private set; }
        //}
        //public class Reply : IEnumerable
        //{
        //    private readonly char[] _reply;
        //    public Reply(char[] reply)
        //    {
        //        _reply = reply;
        //    }
        //    public IEnumerator GetEnumerator()
        //    {
        //        return new ReplyEnumerator(_reply);
        //    }
        //}
    }
}