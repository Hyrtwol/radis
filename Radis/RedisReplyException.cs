﻿using System;

namespace Radis
{
    public class RedisReplyException : RedisException
    {
        //public RedisReplyException(char[] reply) : base(new string(reply)) {}

        public RedisReplyException(char[] reply, Exception exception)
            : base(new string(reply), exception)
        {
        }
    }
}