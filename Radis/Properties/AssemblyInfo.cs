using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Radis")]
[assembly: AssemblyDescription("Redis client")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Radis")]
[assembly: AssemblyCopyright("Copyright Hyrtwol 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: InternalsVisibleTo("Radis.Test")]
[assembly: ComVisible(false)]
[assembly: Guid("af1339c0-5d11-4024-98de-c05fa8787392")]

[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.1.0.0")]
