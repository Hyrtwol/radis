﻿using System;

namespace Radis
{
    public class RedisException : Exception
    {
        public RedisException(string message)
            : base(message)
        {
        }

        public RedisException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}