﻿using System;
using System.Runtime.InteropServices;

namespace Radis
{
    public static class ConsoleUtils
    {
        private const int SWP_NOSIZE = 0x0001;
        private static readonly IntPtr ThisConsole = GetConsoleWindow();

        public static void SetWindowPos(int left, int top)
        {
            SetWindowPos(ThisConsole, 0, left, top, 0, 0, SWP_NOSIZE);
        }

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        private static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
    }
}
